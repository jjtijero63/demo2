package com.techu.labrest.apisaludos.demo;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("/api/v1/saludos")
public class ControladorSaludos {

    //Pojo plain old java
    static class Saludo
    {
       public   long id;
          // Atributo publico
        public  String  saludoCorto;
       public String  saludoLargo ;
       public String url;



    }

    private final AtomicLong secuencuiaIds= new AtomicLong(0L);

    private final List<Saludo> saludos = new LinkedList<Saludo>();

    @GetMapping("/")
    public List<Saludo>  obtenerTodosSaludos()
    {
        return this.saludos;
    }


    @GetMapping("/{id}")
    public Saludo obtenerUnSaludos(@PathVariable long id)
    {
        for(Saludo saludo: this.saludos)
        {
            if(id==saludo.id)
            {
                return saludo;
            }
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }


    @PostMapping("/")
    public Saludo agregarSaludo(@RequestBody Saludo saludo)
    {
        saludo.id = this.secuencuiaIds.incrementAndGet();
          this.saludos.add(saludo);
          return saludo;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borradoSaludo(@PathVariable long id)
    {
       for(int i=0;i< this.saludos.size();++i)

       {
           Saludo saludo= this.saludos.get(i);
           if(id==saludo.id)
           {
               this.saludos.remove(i);
               return;
           }
       }

    }






 @PutMapping("/{id}")
   public void reemplazarSaludo(@PathVariable long id, @RequestBody Saludo saludoNuevo )
   {
       for (int i=0; i<saludos.size();i++)
       {
           Saludo saludo = this.saludos.get(i);
           if (saludo.id==id)
           {
               //this.saludos.set(i,saludoNuevo);
               saludo.saludoCorto=saludoNuevo.saludoCorto;
               saludo.saludoLargo=saludoNuevo.saludoLargo;
               return;
           }
       }
       throw new ResponseStatusException(HttpStatus.NOT_FOUND);

   }

   static class ParcheSaludo
   {
      public String operacion; //{borra atributo, modificar - atrinuto}
      public String atributo; // Cual atributo borro;
      public Saludo saludo; //los atributos modificados para modificar-atributos


   }



@PatchMapping("/{id}")
 public void modificarSaludo(@PathVariable long id, @RequestBody ParcheSaludo parche)
 {
     for (int i=0; i<saludos.size();i++)
     {
         Saludo saludo = this.saludos.get(i);
         if (saludo.id==id)
         {
            this.saludos.set(i, emparcharSaludo(saludo,parche));
             return;
         }
     }


     throw  new ResponseStatusException(HttpStatus.NOT_FOUND);
 }

 private Saludo emparcharSaludo(Saludo saludo,ParcheSaludo parche)
 {
     if(parche.operacion.trim().equalsIgnoreCase("borrar-atributo"))
     {
         if("saludoCorto".equalsIgnoreCase(parche.atributo))
         {
             saludo.saludoCorto=null;

         }

         if("saludoLargo".equalsIgnoreCase(parche.atributo))
         {
             saludo.saludoLargo=null;

         }
     } else if (parche.operacion.trim().equalsIgnoreCase("modificar-atributo"))
     {
         if(parche.saludo.saludoCorto!=null)
         {
             saludo.saludoCorto=parche.saludo.saludoCorto;
         }

         if(parche.saludo.saludoLargo!=null)
         {
             saludo.saludoLargo=parche.saludo.saludoLargo;
         }
     }

     return saludo;
 }




}
